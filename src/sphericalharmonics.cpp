///
/// \file sphericalharmonics.cpp
/// \brief Definitions of functions related to SphericalHarmonic and SHBases.
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include "wigxjpf.h"
#include "sphericalharmonics.h"

/// \brief The larges spinweight occuring in the calculations is s=4 (or s=-4 in the complex conjugate)
const int maxs{4};

SphericalHarmonic::SphericalHarmonic(const int s, const int l, const int m, const int imax, const int lmax): m_imax{imax}, m_lmax{lmax}
{
    m_Ys = s;
    m_Ym = m;
    m_Yl = l;
    // allocating memory for the coefficients
    resizeMatrix();

    // setting up wigxjpf
    // the larges possible l is math_lmax, and we want 3j symbols
    wig_table_init(2*lmax, 3);
    // single process precomputation
    wig_temp_init(2*lmax);
    // Iterating through the spin-weigth of input function.
    for(int s2 = -maxs; s2 <= maxs; ++s2)
    {
        // Iterating through the composit index of input function.
        for(int ii = 0; ii <= imax; ++ii)
        {
            // Iterating through the composit index of output function.
            for(int io = 0; io <= imax; ++io)
            {
                // have to think about the correct place for itol
                int l2=composit2Yindexl(ii);
                int m2=composit2Yindexm(ii,l2);
                int l3=composit2Yindexl(io);
                // the sign is to absorb the complex conjugate
                // io is the index of the result
                int m3=-composit2Yindexm(io,l3);

                if(isGauntStored(s,s2,l,l2,l3,m,m2,m3))
                {
                    GReal_t coupling = std::sqrt((2.0*l+1.0)*(2.0*l2+1.0)*(2.0*l3+1.0))*Constants::i2sqrtpi
                                            *wig3jj(l*2, l2*2, l3*2, -s*2, -s2*2, -(-s-s2)*2)
                                            *wig3jj(l*2, l2*2, l3*2, m*2, m2*2, m3*2);
                    putGaunt(coupling,l,l2,l3,m2,s2);
                }
            }
        }
    }
    // tearing down wigxjpf
    wig_temp_free();
    wig_table_free();
}

void SphericalHarmonic::putGaunt(const GReal_t gc, const int l1, const int l2, const int l3, const int m2, const int s2)
{
    // conversion of multipole indices to arrey indicies
    // l3 goes from |l1-l2| to l1+l2 so the indexing is done by
    int l3i = Y2GGGindexl3(l1,l2,l3);
    // s2 goes from -min(l2,l3) to min(l2,l3) so the indexing is done by
    int s2i = Y2GGGindexsm2(s2,l2,l3);
    // m2 goes from -min(l2,l3) to min(l2,l3) so the indexing is done by
    int m2i = Y2GGGindexsm2(m2,l2,l3);

    m_GGG.at(l2).at(l3i).at(s2i).at(m2i)=gc;
}

bool SphericalHarmonic::isGauntVanishing(const int s1, const int s2, const int l1, const int l2, const int l3, const int m1, const int m2, const int m3) const
{
    // if |s_i|>l_i we are bound to get 0
    if(std::abs(s1)>l1 || std::abs(s2)>l2 || std::abs(s1+s2)>l3)
    {
        return true;
    }
    // similarly if |m_i|>l_i we are bound to get 0
    if(std::abs(m1)>l1 || std::abs(m2)>l2 || std::abs(m3)>l3)
    {
        return true;
    }
    // if m1+m2+m3\neq0 we are bound to get 0
    if(m1+m2+m3!=0)
    {
        return true;
    }
    // the same reasoning for s_i is built into the storage structure
    // if l3<|l1-l2| or l3>l1+l2 we are bound to get 0
    if(l3<std::abs(l1-l2) || l3>l1+l2)
    {
        return true;
    }
    return false;
}

bool SphericalHarmonic::isGauntStored(const int s1, const int s2, const int l1, const int l2, const int l3, const int m1, const int m2, const int m3) const
{
    // we do not store zeroes and also exploit the permutation symmetry of the problem
    return (!isGauntVanishing(s1,s2,l1,l2,l3,m1,m2,m3))&&(l2<=l3);
}

GReal_t SphericalHarmonic::getGaunt(const int si, const int ii, const int io) const
{
    return innerGetGaunt(m_Yl, si, ii, io);
}

void SphericalHarmonic::resizeMatrix()
{
    // l2 goes from 0 to lmax
    int l2size=m_lmax+1;
    m_GGG.resize(l2size);
    // I think resize does not throw exception when there is not enough memory
    if(static_cast<int>(m_GGG.size())<l2size)
    {
        throw std::string("Could not allocate memory to m_GGG");
    }
    for(int l2=0; l2<l2size; ++l2)
    {
        // l3 goes from |l1-l2| to l1+l2 wich gives 2*min(l1,l2)+1 possible values
        // or until lmax
        int l3size=std::min(2*std::min(m_Yl,l2)+1,m_lmax+1-std::abs(m_Yl-l2));
        m_GGG.at(l2).resize(l3size);
        if(static_cast<int>(m_GGG.at(l2).size())<l3size)
        {
            throw std::string("Could not allocate memory to m_GGG");
        }
        for(int l3i=0; l3i<l3size; ++l3i)
        {
            // get l3 from the index position
            int l3=l3i+std::abs(m_Yl-l2);
            // s2 goes from -min(l2,l3) to min(l2,l3) which gives 2*min(l2,l3)+1 possible values
            int s2size=2*std::min(l2,l3)+1;
            // m2 also goes from -min(l2,l3) to min(l2,l3)
            int m2size=2*std::min(l2,l3)+1;
            m_GGG.at(l2).at(l3i).resize(s2size);
            if(static_cast<int>(m_GGG.at(l2).at(l3i).size())<s2size)
            {
                throw std::string("Could not allocate memory to m_GGG");
            }
            for(int s2i=0; s2i<s2size; ++s2i)
            {
                m_GGG.at(l2).at(l3i).at(s2i).resize(m2size);
                if(static_cast<int>(m_GGG.at(l2).at(l3i).at(s2i).size())<m2size)
                {
                    throw std::string("Could not allocate memory to m_GGG");
                }
            }
        }
    }
}

GReal_t SphericalHarmonic::innerGetGaunt(const int l, const int si, const int ii, const int io) const
{
    // getting s,l,m with taking the complex conjugate in the integral
    // the phase factors are dealt with in the multiplication (func.cpp)
    int s2=si;
    int l2=composit2Yindexl(ii);
    int m2=composit2Yindexm(ii,l2);
    int s3=-(m_Ys+s2);
    int l3=composit2Yindexl(io);
    int m3=-composit2Yindexm(io,l3);
    // if l3<l2 we apply a permutation
    if(l3<l2)
    {
        s2=s3;
        int tmp=l3;
        l3=l2;
        l2=tmp;
        tmp=m3;
        m3=m2;
        m2=tmp;
    }
    // getting matrix indices
    int l3i = Y2GGGindexl3(l,l2,l3);
    int s2i = Y2GGGindexsm2(s2,l2,l3);
    int m2i = Y2GGGindexsm2(m2,l2,l3);

    if(isGauntVanishing(m_Ys,s2,m_Yl,l2,l3,m_Ym,m2,m3))
    {
        return 0;
    }
    else
    {
        return m_GGG[l2][l3i][s2i][m2i];
    }
}

SHBases::SHBases(const int lmax): m_lmax{lmax}, m_imax{computeIMax(lmax)}, m_SH((2*maxs+1)*(m_imax+1))
{
    for(int s = -maxs; s <= maxs; ++s)
    {
        for(int i = 0; i <= m_imax; ++i)
        {
            int m{itom(i)};
            int l{itol(i)};
            int ix = (s+maxs)*(m_imax+1)+i;
            m_SH.at(ix)=SphericalHarmonic(s, l, m, m_imax, m_lmax);
        }
    }
}

const SphericalHarmonic& SHBases::getY(const int s, const int i) const
{
    int ix = (s+maxs)*(m_imax+1)+i;
    return m_SH.at(ix);
}
