///
/// \file func.cpp
/// \brief Implementation of the functions related to class Function.
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <sstream>
#include "func.h"

/// The resulting function is 0 with s=Ys. It is useful before reading data.
Function::Function(const int Ys, const SHBases &SHB): m_SHB{SHB}, m_Ys{Ys}
{
    m_coeff = std::vector<Complex>(arrayLength(m_SHB.lMax()),0);
}

/// Compares the length of the vector containing the coefficients and the expected length. If differs throws an exception.
/// This constructor is mainly for testing purposes.
Function::Function(const std::vector<Complex> &coeffs, const int Ys, const SHBases &SHB)
: m_SHB{SHB}, m_Ys{Ys}
{
    if(static_cast<int>(coeffs.size())!=(arrayLength(m_SHB.lMax())))
    {
        throw std::string("Failed constructing Function because of inappropriate size of given array");
    }
    else
    {
        m_coeff = coeffs;
    }
}
/// There is nothing here to explain.
Function::Function(const Function &ff): m_SHB{ff.m_SHB}, m_Ys{ff.m_Ys}
{
    m_coeff = ff.m_coeff;
}
/// First checks if the desired component is physical (\f$l \geq |s|\f$), then creates a vector with the specified length.
/// If \f$l=0\f$ the constructor automatically takes account of the factor \f$2\pi\f$ (because f=(f/Y00)\cdot Y00), so we could just type in the \f$\vartheta\f$ independent functions.
Function::Function(const Complex val, const int s, const int l, const int m, const SHBases &SHB): m_SHB{SHB}, m_Ys{s}
{
    if(l<std::abs(s))
        throw std::string("Unphysical coefficient (l = ")+std::to_string(l)+std::string(" < |s|, s = ")+std::to_string(s);
    if(l<std::abs(m))
        throw std::string("Unphysical coefficient (l = ")+std::to_string(l)+std::string(" < |m|, m = ")+std::to_string(m);

    std::vector<Complex> tmp(arrayLength(m_SHB.lMax()),0);
    tmp[lmtoi(l,m)] = (l==0) ? val/Constants::i2sqrtpi : val;
    m_coeff = tmp;
}
/// Operator<< passes a Function to a stream element by element separated by tabulator.
std::ostream& operator<<(std::ostream &out, const Function &ff)
{
    for(unsigned i=0; i<ff.m_coeff.size(); ++i)
    {
        out << ff.m_coeff[i] << '\t';
    }

    return out;
}
/// Operator>> gets Complex numbers until the slots in Function are full.
std::istream& operator>>(std::istream &in, Function &ff)
{
    for(unsigned i=0; i<ff.m_coeff.size(); ++i)
    {
        in >> ff.m_coeff[i];
    }

    return in;
}
/// Since I not implemented direct conversion from Complex to Function it can not work as multiplication from right.
Function operator*(const Complex factor, const Function &ff)
{
    Function tmp(ff);
    for(unsigned i=0; i<ff.m_coeff.size(); ++i)
    {
        tmp.m_coeff[i] = factor*ff.m_coeff[i];
    }
    return tmp;
}
/// Simply calls the operator of multiplication from left.
Function operator*(const Function &ff, const Complex factor)
{
    return Function(factor*ff);
}
/// Division element by element.
Function operator/(const Function &ff, const Complex denominator)
{
    Function tmp(ff);
    for(unsigned i=0; i<ff.m_coeff.size(); ++i)
    {
        tmp.m_coeff[i] = ff.m_coeff[i]/denominator;
    }
    return tmp;
}
/// Addition element by element. If functions of different spin-weights are added then throws an error.
Function operator+(const Function &f1, const Function &f2)
{
    if((f1.m_Ys!=f2.m_Ys)||(f1.m_coeff.size()!=f2.m_coeff.size()))
    {
        throw std::string("Addition of incompatible functions");
    }
    Function tmp(f1);
    for(unsigned i=0; i<f1.m_coeff.size(); ++i)
    {
        tmp.m_coeff[i] = f1.m_coeff[i] + f2.m_coeff[i];
    }
    return tmp;
}
/// Substraction element by element. If functions of different spin-weights are added then throws an error.
Function operator-(const Function &f1, const Function &f2)
{
    if((f1.m_Ys!=f2.m_Ys)||(f1.m_coeff.size()!=f2.m_coeff.size()))
    {
        throw std::string("Substraction of incompatible functions");
    }
    Function tmp(f1);
    for(unsigned i=0; i<f1.m_coeff.size(); ++i)
    {
        tmp.m_coeff[i] = f1.m_coeff[i] - f2.m_coeff[i];
    }
    return tmp;
}
/// Get the coefficients of the product by using the Gaunt-coefficients.
/// \f[
///     {}_sY_l{}^0\cdot f=\sum_{l'=0}^{l_{max}}{}_sY_l{}^0 {}_{s'}f_{l'}{}^0 {}_{s'}Y_{l'}{}^0 =
///     \sum_{l''=|l-l'|}^{\mathrm{min}(l+l',l_{max})} {}_{s'}f_{l'}{}^0 (-1)^{s+s'} {}_{s,s',-s-s'}G_l^0{}_{l'}^0{}_{l''}^0 {}_{s+s'}Y_{l''}{}^0
/// \f]
Function operator*(const SphericalHarmonic &Y, const Function &f)
{
    Function tmp(f.m_Ys+Y.getS(),f.m_SHB);
    int signs = (tmp.m_Ys % 2) ? -1 : 1;
    for(unsigned i=0; i<tmp.m_coeff.size(); ++i)
    {
        int im{tmp.itom(i)};
        int signm{(im % 2) ? -1 : 1};
        int m{im-Y.getM()};
        for(int l=std::abs(m); l<=f.m_SHB.lMax(); ++l)
        {
            int j{f.lmtoi(l,m)};
            tmp.m_coeff[i] = tmp.m_coeff[i] + signm*signs*Y.getGaunt(f.m_Ys,j,i)*f.m_coeff[j];
        }
    }
    return tmp;
}
/// Take the expansion of the first function and reduce the problem to multiplication by spherical harmonics.
/// \f[
///     f\cdot g = \sum_{l=0}^{l_{max}}{}_sf_l{}^0\cdot {}_sY_l{}^0\times g
/// \f]
Function Function::operator*(const Function &f) const
{
    if(m_coeff.size() != f.m_coeff.size())
        throw std::string("Multiplication of vectors with different sizes");
    Function tmp(f.m_Ys+m_Ys,m_SHB);

    for(int i = 0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        tmp = tmp + m_coeff.at(i)*(m_SHB.getY(m_Ys,i)*f);
    }

    return tmp;
}
/// This operator is basically a wrapper around the function divideByNeumann. The source code of GridRipper was especially helpful for understanding the concept.
Function operator/(const Function &f1, const Function &f2)
{
    std::ofstream debug("debug.txt",std::ios_base::app);
    Complex scale  = f2.optimalScalingNeumann();
    Function scaledDenominator = f2/scale;
    GReal_t norm   = scaledDenominator.normBoundNeumann();
    debug << "norm = " << norm << std::endl;

    Function result = divideByNeumann(scaledDenominator,f1,norm,Constants::neumannTolerance)/scale;

    return result;
}
/// Function times -1.
Function Function::operator-() const
{
    return -1*(*this);
}
/// Assign coefficients of another Function. Can not equate Functions with different spin-weight.
Function& Function::operator=(const Function &f)
{
    if(m_Ys != f.m_Ys)
        throw std::string("Can not equate two functions with different spin-weight!");

    m_coeff = f.m_coeff;

    return *this;
}
/// Exact comparison for whatever reason.
/// Note that the size of the two Functions assumed to be the same.
bool operator!=(const Function &f1, const Function &f2)
{
    if(f1.m_Ys != f2.m_Ys)
    {
        return true;
    }
    for(int i = 0; i < static_cast<int>(f1.m_coeff.size()); ++i)
    {
        if(f1.m_coeff[i] != f2.m_coeff[i])
        {
            return true;
        }
    }
    return false;
}
/// Floating point comparison for unit testing.
/// Note that the size of the two Functions assumed to be the same.
bool operator==(const Function &f1, const Function &f2)
{
    bool isSpinweightOK{false};
    if(f1.m_Ys == f2.m_Ys)
    {
        isSpinweightOK=true;
    }
    bool isCoefficentsOK{true};
    for(int i = 0; i < static_cast<int>(f1.m_coeff.size()); ++i)
    {
        GReal_t err;
        if(f1.m_coeff[i].abs() == 0)
        {
            err=(f1.m_coeff[i]-f2.m_coeff[i]).abs();
        }
        else
        {
            err=((f1.m_coeff[i]-f2.m_coeff[i])/f1.m_coeff[i]).abs();
        }
        if(err > 1e-15)
        {
            isCoefficentsOK=false;
        }
    }
    return isSpinweightOK&&isCoefficentsOK;
}
/// Do not forget the \f$(-1)^{s+m}\f$ factor from the spherical harmonics!
/// \f[
///    \overline{f}=\overline{\sum {}_sf_l{}^0 {}_sY_l{}^0}=\sum \overline{{}_sf_l{}^0} (-1)^s {}_{-s}Y_l{}^0
/// \f]
Function Function::bar() const
{
    Function tmp(-m_Ys,m_SHB);
    int sign = (m_Ys%2) ? -1 : 1;
    for(int i=0; i<static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        int m{itom(i)};
        int signm{m % 2 ? -1 : 1};
        int ii{lmtoi(l,-m)};
        tmp.m_coeff[ii]   =  signm*sign*m_coeff[i].conjugate();
    }
    return tmp;
}

/// The Newman-Penrose \f$\eth\f$ operator acts on the spin-weighted spherical harmonics as
/// \f[
///   \eth {}_sY_l{}^m=\sqrt{(l-s)(l+s+1)}{}_{s+1}Y_l{}^m.
/// \f]
Function Function::eth() const
{
    Function tmp(m_Ys+1,m_SHB);
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        if(l < std::abs(tmp.m_Ys))
        {
            tmp.m_coeff[i]  = 0;
        }
        else
        {
            tmp.m_coeff[i]  = std::sqrt((l-m_Ys)*(l+m_Ys+1))*m_coeff[i];
        }
    }
    return tmp;
}

/// The Newman-Penrose \f$\bar{\eth}\f$ operator acts on the spin-weighted spherical harmonics as
/// \f[
///   \bar{\eth} {}_sY_l{}^m=-\sqrt{(l+s)(l-s+1)}{}_{s-1}Y_l{}^m.
/// \f]
Function Function::ethbar() const
{
    Function tmp(m_Ys-1,m_SHB);
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        if(l < std::abs(tmp.m_Ys))
        {
            tmp.m_coeff[i]  = 0;
        }
        else
        {
            tmp.m_coeff[i]  = -std::sqrt((l+m_Ys)*(l-m_Ys+1))*m_coeff[i];
        }
    }
    return tmp;
}

/// The Newman-Penrose \f$\bar{\eth}\eth\f$ operator acts on the spin-weighted spherical harmonics as
/// \f[
///   \bar{\eth}\eth {}_sY_l{}^m=-(l-s)(l+s+1){}_{s}Y_l{}^m.
/// \f]
Function Function::ethbareth() const
{
    Function tmp(m_Ys,m_SHB);
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        if(l < std::abs(tmp.m_Ys))
        {
            tmp.m_coeff[i]  = 0;
        }
        else
        {
            tmp.m_coeff[i]  = -(l-m_Ys)*(l+m_Ys+1)*m_coeff[i];
        }
    }
    return tmp;
}

/// The Newman-Penrose \f$\eth\bar{\eth}\f$ operator acts on the spin-weighted spherical harmonics as
/// \f[
///   \eth\bar{\eth} {}_sY_l{}^m=(\bar{\eth}\eth -2s)\cdot {}_sY_l{}^m=-(l+s)(l-s+1){}_{s}Y_l{}^m.
/// \f]
Function Function::ethethbar() const
{
    Function tmp(m_Ys,m_SHB);
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        if(l < std::abs(tmp.m_Ys))
        {
            tmp.m_coeff[i]  = 0;
        }
        else
        {
            tmp.m_coeff[i]  = -(l+m_Ys)*(l-m_Ys+1)*m_coeff[i];
        }
    }
    return tmp;
}

/// The Newman-Penrose \f$\bar{\eth}^2\f$ operator acts on the spin-weighted spherical harmonics as
/// \f[
///   \bar{\eth}^2 {}_sY_l{}^m=\sqrt{(l+s)(l-s+1)(l+s-1)(l-s+2)} {}_{s-2}Y_l{}^m.
/// \f]
Function Function::ethbar2() const
{
    Function tmp(m_Ys-2,m_SHB);
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        if(l < std::abs(tmp.m_Ys))
        {
            tmp.m_coeff[i]  = 0;
        }
        else
        {
            tmp.m_coeff[i]  = std::sqrt((l+m_Ys)*(l-m_Ys+1)*(l+m_Ys-1)*(l-m_Ys+2))*m_coeff[i];
        }
    }
    return tmp;
}
Function Function::re() const
{
    Function tmp(0,m_SHB);
    if(m_Ys!=0)
    {
        throw std::string("re() is defined only for s=0");
    }
    for(int i=0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        int m{itom(i)};
        if(m==0)
        {
            tmp.m_coeff[i] = m_coeff[i].real();
        }
        else
        {
            int sgn = m%2 ? -1 : 1;
            tmp.m_coeff[i]  = 0.5*(m_coeff[i] + sgn*m_coeff[lmtoi(l,-m)].conjugate());
        }
    }
    return tmp;
}
/// The norm is defined by
/// \f[
/// ||f||=\frac{\sqrt{\sum |f_l|^2}}{\sqrt{4\pi}}.
/// \f]
GReal_t Function::norm() const
{
    GReal_t sum{0};
    for(int i = 0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        sum += m_coeff[i].abs2();
    }
    return std::sqrt(sum)*Constants::i2sqrtpi;
}
/// The norm is computed as
/// \f[
///    ||F||_{H_2^2}=\sqrt{\sum_i S_2(\lambda_i)\cdot |\langle Y_i,1\rangle-\langle Y_i,F\rangle|^2},
/// \f]
/// where \f$\lambda_i\f$ is the eigenvalue of Laplace-operator and
/// \f[
///    S_2(z) = \sum_{l=0}^2 (-z)^l.
/// \f]
GReal_t Function::SobolevNorm2() const
{
    GReal_t result = 0;
    // Parameter in Sobolev norm.
    GReal_t mu = 2;
    for(int i = 0; i < static_cast<int>(m_coeff.size()); ++i)
    {
        int l{itol(i)};
        result += (std::pow((l*(l+1.0)), (mu+1))-1.0)/(l*(l+1)-1.0)*m_coeff[i].abs2();
    }
    return result;
}
/// The optimal scaling factor is computed as
/// \f[
///    z = \frac{1}{Y_0}\frac{\overline{\langle Y_0,F\rangle}_{L_2}}{||F||^2_{H_2^2}}
/// \f]
/// We compute \f$1/z\f$.
Complex Function::optimalScalingNeumann() const
{
    Complex f00(m_coeff[0]);
    GReal_t norm = (*this).SobolevNorm2();
    Complex tmp = norm*Constants::i2sqrtpi/(f00.abs2())*f00;
    return tmp;
}

GReal_t Function::normBoundNeumann() const
{
    Complex a00 = m_coeff[0];
    GReal_t H2S2aa = (*this).SobolevNorm2();
    return SobolevConstant()*std::sqrt(std::fabs(4*Constants::pi-4.0*std::sqrt(Constants::pi)*a00.real()+(H2S2aa)));
}
/// The square of the Sobolev constant is
/// \f[
///    K^2 = \frac{1}{4\pi}\sum_{l=0}^{\infty}\frac{(2l+1)(l(l+1)-1)}{(l(l+1))^{r+1}-1}
/// \f]
GReal_t SobolevConstant()
{
    /// Do not compute Sobolev constant! Look up the value from the paper or compute it once using mathematica!
//     GReal_t result=0;
//     int mu = 2;
// 	for(int l = 0; l <= SobolevOrderS2; ++l)
// 	{
// 	    result+=(2*l+1)*(l*(l+1)-1.0)/(std::pow((l*(l+1.0)),(mu+1))-1.0);
// 	}
//	result=std::sqrt(result/(4*Constants::pi));
//    return result;
    return 0.36236006615682591475L;
}
Function divideByNeumann(const Function& a, const Function& f, const GReal_t norm, const GReal_t tolerance)
{
    if(norm < tolerance)
        return f;
    int N = static_cast<int>(std::ceil(std::log(tolerance)/std::log(norm)))-1;
    if(!(std::fabs(tolerance) < 1.0))
    {
        throw std::string("|tolerance|<1 not satisfied.");
    }
    if(!(std::fabs(norm) < 1.0))
    {
        throw std::string("|norm|<1 not satisfied. Norm is ")+std::to_string(norm);
    }
    if(!(N >= 0))
    {
        throw std::string("Negative order in division.");
    }
    Function result(f.m_Ys-a.m_Ys,f.m_SHB);

    for(int i = 0; i < N; ++i)
        result = result + f - a*result;

    return result;
}
Function sqrt(const Function& x)
{
    if(x.spinWeight()!=0)
    {
        throw std::string("Square root is defined for s=0 functions.");
    }
    try
    {
        GReal_t l2norm=x.norm();
        Function xx=x/l2norm;
        return std::sqrt(l2norm)*xx.sqrtTaylor();
    }
    catch(std::string msg)
    {
        //std::cerr << "Computing sqrt with Taylor series failed" << std::endl;
        GReal_t l2norm=x.norm();
        Function xx=x/l2norm;
        //return x.sqrtNewton();
        return std::sqrt(l2norm)*xx.sqrtNewton();
    }
}
bool Function::checkSqrtError(const Function &res, GReal_t &err, GReal_t &preverr) const
{
    preverr=err;
    err = (res*res-*this).norm();

    if(err>=preverr && preverr!=0)
    {
        throw std::string("Iteration in sqrt does not converge");
    }
    return err < Constants::sqrtTolerance;
}
void Function::getNextTwoTerms(Function &res, Function &dxn, const Function &dx, const GReal_t sqrtNorm, GReal_t &TC, GReal_t &order) const
{
    dxn=dxn*dx;
    res=res+sqrtNorm*nextTaylorCoeff(TC,order)*dxn;

    dxn=dxn*dx;
    res=res+sqrtNorm*nextTaylorCoeff(TC,order)*dxn;
}
Function Function::sqrtTaylor() const
{
    GReal_t L2Norm=norm();
    Function dx=*this/L2Norm-Function(1,0,0,0,SHB());
    Function res(0,SHB());
    Function dxn(1,0,0,0,SHB());
    GReal_t sqrtNorm=std::sqrt(L2Norm);

    GReal_t err=0;
    GReal_t preverr=0;
    GReal_t TC=1;
    GReal_t order=0;

    res=Function(sqrtNorm,0,0,0,SHB());

    while(!checkSqrtError(res,err,preverr))
    {
        getNextTwoTerms(res, dxn, dx, sqrtNorm, TC, order);
    }
    return res;
}
Function Function::sqrtTaylorIterative() const
{
    Function res(0,SHB());

    GReal_t err=0;
    GReal_t preverr=0;

    res=(*this)/2;

    while(!checkSqrtError(res,err,preverr))
    {
        Taylor4(res);
    }
    return res;
}
void Function::Taylor4(Function &res) const
{
    Function d=(*this)-res*res;
    Function xi=0.5*d/(res*res);
    Function ID(1,0,0,0,m_SHB);

    res = res*(ID+xi-0.5*xi*xi+0.5*xi*xi*xi-5*xi*xi*xi*xi/8);
}
GReal_t Function::nextTaylorCoeff(GReal_t &TaylorCoeff, GReal_t &previousOrder) const
{
    TaylorCoeff = 0.5*(1-2*previousOrder)/(1+previousOrder)*TaylorCoeff;
    previousOrder++;
    return TaylorCoeff;
}
Function Function::sqrtNewton() const
{
    GReal_t L2Norm=norm();
    Function res(0,SHB());
    GReal_t sqrtNorm=std::sqrt(L2Norm);

    GReal_t err=0;
    GReal_t preverr=0;

    res=Function(sqrtNorm,0,0,0,SHB());

    while(!checkSqrtError(res,err,preverr))
    {
        res=(res+*this/res)/2;
        res=(res+*this/res)/2;
    }
    return res;
}
Function eth(const Function& x)
{
    return x.eth();
}
Function ethbar(const Function& x)
{
    return x.ethbar();
}
Function ethbareth(const Function& x)
{
    return x.ethbareth();
}
Function ethethbar(const Function& x)
{
    return x.ethethbar();
}
Function ethbar2(const Function& x)
{
    return x.ethbar2();
}
Function Re(const Function& x)
{
    return x.re();
}
Function Abs2(const Function& x)
{
    return Re(x*x.bar());
}
