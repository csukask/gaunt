///
/// \file complex.cpp
/// \brief Implementation of the functions related to class Complex.
#include <cmath>
#include "complex.h"

Complex::Complex(const GReal_t re, const GReal_t im): m_re{re}, m_im{im}
{}

Complex::Complex(const Complex &c): m_re{c.m_re}, m_im{c.m_im}
{}

std::ostream& operator<<(std::ostream &out, const Complex &c)
{
    out << c.m_re << " " << c.m_im;
    return out;
}
std::istream& operator>>(std::istream &in, Complex &c)
{
    in >> c.m_re;
    in >> c.m_im;
    return in;
}

Complex& Complex::operator=(const Complex &c)
{
    if(this == &c)
    {
        return *this;
    }
    m_re=c.m_re;
    m_im=c.m_im;
    return *this;
}

Complex operator+(const Complex &c1, const Complex &c2)
{
    return Complex(c1.m_re+c2.m_re, c1.m_im+c2.m_im);
}

Complex operator-(const Complex &c1, const Complex &c2)
{
    return Complex(c1.m_re-c2.m_re, c1.m_im-c2.m_im);
}

Complex operator*(const Complex &c1, const Complex &c2)
{
    return Complex(c1.m_re*c2.m_re-c1.m_im*c2.m_im, c1.m_im*c2.m_re+c1.m_re*c2.m_im);
}

Complex operator/(const Complex &c1, const Complex &c2)
{
    GReal_t z = c2.m_re*c2.m_re + c2.m_im*c2.m_im;
    return Complex((c1.m_re*c2.m_re+c1.m_im*c2.m_im)/z, (c1.m_im*c2.m_re-c1.m_re*c2.m_im)/z);
}

Complex Complex::operator-() const
{
    return Complex(-m_re,-m_im);
}

bool operator!=(const Complex &c1, const Complex &c2)
{
    return (c1.m_re != c2.m_re)||(c1.m_im != c2.m_im);
}

Complex Complex::conjugate() const
{
    return Complex(m_re,-m_im);
}

GReal_t Complex::abs() const
{
    return std::sqrt(m_re*m_re+m_im*m_im);
}

GReal_t Complex::abs2() const
{
    return m_re*m_re+m_im*m_im;
}
