#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "func.h"

TEST_CASE("GauntMultiplication","[GauntMultiplication]")
{
    const SHBases SHB(5);
    // Function(val,s,l,m,&SHBases)
    // some spherical harmonics
    const Function Y000(Constants::i2sqrtpi,0,0,0,SHB);
    const Function Ym110(1,-1,1,0,SHB);
    const Function Y010(1,0,1,0,SHB);
    const Function Y110(1,1,1,0,SHB);
    // zero with different spin-weights
    const Function sm1zero(0,-1,1,0,SHB);
    const Function s0zero(0,0,0,0,SHB);
    const Function s1zero(0,1,1,0,SHB);

    // zero times anything
    REQUIRE(s0zero*Y000 == s0zero);
    REQUIRE(s0zero*Ym110 == sm1zero);
    REQUIRE(s0zero*Y010 == s0zero);
    REQUIRE(s0zero*Y110 == s1zero);
    // constant times anything
    const Function g2sm1 = Function(1,-1,2,0,SHB)+Function(std::sqrt(2),-1,3,0,SHB);
    const Function g2s1 = Function(1,1,2,0,SHB)+Function(std::sqrt(2),1,3,0,SHB);
    const Function g4 = Function(5,0,4,0,SHB)+Function(6,0,5,0,SHB);
    REQUIRE(g2s1*Y000 == Constants::i2sqrtpi*g2s1);
    REQUIRE(g2sm1*Y000 == Constants::i2sqrtpi*g2sm1);
    REQUIRE(g4*Y000 == Constants::i2sqrtpi*g4);
    // multiplications without cutoff
    const Function g2bsm1 = Function(1,-1,2,0,SHB);
    const Function g2bs1 = Function(1,1,2,0,SHB);
    const Function G0s1 = Function(0.5*std::sqrt(3.0/5/Constants::pi),1,1,0,SHB)+Function(std::sqrt(6.0/35/Constants::pi),1,3,0,SHB);
    const Function G0sm1 = Function(0.5*std::sqrt(3.0/5/Constants::pi),-1,1,0,SHB)+Function(std::sqrt(6.0/35/Constants::pi),-1,3,0,SHB);
    const Function G1s1 = Function(0.5*std::sqrt(3.0/5/Constants::pi),1,1,0,SHB)
                         +Function(2*std::sqrt(3.0/35/Constants::pi),1,2,0,SHB)
                         +Function(std::sqrt(6.0/35/Constants::pi),1,3,0,SHB)
                         +Function(std::sqrt(5.0/14/Constants::pi),1,4,0,SHB);
    REQUIRE(g2bs1*Y010 == G0s1);
    REQUIRE(g2bsm1*Y010 == G0sm1);
    REQUIRE(g2s1*Y010 == G1s1);
    // multiplications with cutoff at lower l
    const Function g3sm1 = Function(3,-1,1,0,SHB)+Function(42,-1,2,0,SHB);
    const Function g3s0 = Function(0.25/Constants::pi/std::sqrt(Constants::pi),0,0,0,SHB)+Function(3,0,1,0,SHB)+Function(42,0,2,0,SHB);
    const Function g3s1 = Function(3,1,1,0,SHB)+Function(42,1,2,0,SHB);
    const Function G2s0 = Function(0.75/Constants::pi,0,0,0,SHB)
                         +Function(0.25/Constants::pi/std::sqrt(Constants::pi),0,1,0,SHB)
                         +Function(42/std::sqrt(5.0*Constants::pi),0,1,0,SHB)
                         +Function(3/std::sqrt(5.0*Constants::pi),0,2,0,SHB)
                         +Function(9*std::sqrt(21.0/5/Constants::pi),0,3,0,SHB);
    const Function G2s1 = Function(21*std::sqrt(3.0/5/Constants::pi),1,1,0,SHB)
                         +Function(1.5*std::sqrt(3.0/5/Constants::pi),1,2,0,SHB)
                         +Function(6*std::sqrt(42.0/5/Constants::pi),1,3,0,SHB);
    const Function G2sm1 = Function(21*std::sqrt(3.0/5/Constants::pi),-1,1,0,SHB)
                         +Function(1.5*std::sqrt(3.0/5/Constants::pi),-1,2,0,SHB)
                         +Function(6*std::sqrt(42.0/5/Constants::pi),-1,3,0,SHB);
    REQUIRE(g3s0*Y010 == G2s0);
    REQUIRE(g3s1*Y010 == G2s1);
    REQUIRE(g3sm1*Y010 == G2sm1);
    // then the product would have higher modes not represented
    const Function g4sm1 = Function(5,-1,4,0,SHB)+Function(6,-1,5,0,SHB);
    const Function g4s1 = Function(5,1,4,0,SHB)+Function(6,1,5,0,SHB);
    const Function g4b(1,1,5,0,SHB);
    const Function G3s1(std::sqrt(2./11/Constants::pi),1,4,0,SHB);
    const Function G4s1 = Function(2.5*std::sqrt(5.0/7/Constants::pi),1,3,0,SHB)
                         +Function(6*std::sqrt(2.0/11/Constants::pi),1,4,0,SHB)
                         +Function(5*std::sqrt(2.0/11/Constants::pi),1,5,0,SHB);
    const Function G4sm1 = Function(2.5*std::sqrt(5.0/7/Constants::pi),-1,3,0,SHB)
                         +Function(6*std::sqrt(2.0/11/Constants::pi),-1,4,0,SHB)
                         +Function(5*std::sqrt(2.0/11/Constants::pi),-1,5,0,SHB);
    REQUIRE(g4b*Y010 == G3s1);
    REQUIRE(g4s1*Y010 == G4s1);
    REQUIRE(g4sm1*Y010 == G4sm1);

    // now product of two whole functions (it made more sense in mathematica)
    const Function f1(0,0,0,0,SHB);
    const Function f2(Constants::i2sqrtpi,0,0,0,SHB);
    const Function f3(1,0,1,0,SHB);
    const Function f4(1,0,2,0,SHB);
    // zero times anything
    REQUIRE(f1*g2s1 == s1zero);
    REQUIRE(f1*g4sm1 == sm1zero);
    REQUIRE(f1*g3s0 == s0zero);
    // constant times anything
    REQUIRE(f2*g2s1 == Constants::i2sqrtpi*g2s1);
    REQUIRE(f2*g3sm1 == Constants::i2sqrtpi*g3sm1);
    REQUIRE(f2*g4s1 == Constants::i2sqrtpi*g4s1);
    // no cutoff
    REQUIRE(f3*g2s1 == G1s1);
    REQUIRE(f3*g4s1 == G4s1);
    REQUIRE(f3*g3s1 == G2s1);
    const Function G5a = Function(3.0/std::sqrt(35*Constants::pi),1,1,0,SHB)
                        +Function(std::sqrt(5.0/Constants::pi)/14,1,2,0,SHB)
                        +Function(1.0/std::sqrt(10*Constants::pi),1,3,0,SHB)
                        +Function(std::sqrt(15.0/2/Constants::pi)/7,1,4,0,SHB)
                        +Function(5/std::sqrt(77*Constants::pi),1,5,0,SHB);
    const Function G5b = Function(-1.5/std::sqrt(5*Constants::pi),1,1,0,SHB)
                        +Function(3*std::sqrt(5.0/Constants::pi),1,2,0,SHB)
                        +Function(9.0/std::sqrt(70*Constants::pi),1,3,0,SHB)
                        +Function(3*std::sqrt(30.0/Constants::pi),1,4,0,SHB);
    const Function G5c = Function(5.0*std::sqrt(15.0/2/Constants::pi)/7,1,2,0,SHB)
                        +Function(15*std::sqrt(2.0/77/Constants::pi),1,3,0,SHB)
                        +Function(85*std::sqrt(5.0/Constants::pi)/154,1,4,0,SHB)
                        +Function(9*std::sqrt(5.0/Constants::pi)/13,1,5,0,SHB);
    REQUIRE(f4*g2s1 == G5a);
    REQUIRE(f4*g3s1 == G5b);
    REQUIRE(f4*g4s1 == G5c);
}
