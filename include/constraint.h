///
/// \file constraint.h
/// \brief Definition of global types and declaration of constants.
#ifndef CONSTRAINT_H
#define CONSTRAINT_H

/// \brief GReal_t represents the underlying precision of the simulation. Generally it is set to long double.
typedef long double GReal_t;

/// \brief A namespace for global constants.
namespace Constants
{
    /// Error tolerance for division.
    extern const GReal_t neumannTolerance;
    /// Error tolerance for square root.
    extern const GReal_t sqrtTolerance;
    /// \f$\sqrt{2}\f$
    extern const GReal_t sqrt2;
    /// \f$\pi\f$
    extern const GReal_t pi;
    /// \f$\frac{1}{2\pi}\f$
    extern const GReal_t i2pi;
    /// \f$\frac{1}{2\sqrt{\pi}}\f$
    extern const GReal_t i2sqrtpi;
    /// \f$\sqrt{\frac{42}{5\pi}}\f$
    extern const GReal_t sqrt42o5pi;
    /// \f$\sqrt{\frac{3}{5\pi}}\f$
    extern const GReal_t sqrt3o5pi;
    /// \f$\sqrt{\frac{5}{7\pi}}\f$
    extern const GReal_t sqrt5o7pi;
    /// \f$\sqrt{\frac{2}{11\pi}}\f$
    extern const GReal_t sqrt2o11pi;
    /// \f$\sqrt{\frac{5}{14\pi}}\f$
    extern const GReal_t sqrt5o14pi;
    /// \f$\sqrt{\frac{3}{35\pi}}\f$
    extern const GReal_t sqrt3o35pi;
    /// \f$\sqrt{\frac{6}{35\pi}}\f$
    extern const GReal_t sqrt6o35pi;
    /// \f$\sqrt{\frac{2\pi}{15}}\f$
    extern const GReal_t sqrt2pio15;
    extern const GReal_t sqrt2pio3;
    extern const GReal_t sqrtpio13;
    extern const GReal_t sqrtpio11;
    extern const GReal_t sqrtpio7;
    extern const GReal_t sqrtpio5;
    extern const GReal_t sqrtpio3;
    extern const GReal_t sqrtpi;
}

#endif
