///
/// \file func.h
/// \brief Defining the class Function.
#ifndef FUNC_H
#define FUNC_H

#include <vector>
#include "constraint.h"
#include "sphericalharmonics.h"
#include "complex.h"

/// \brief Storing multipole coefficients.
///
/// Represents a \f$\vartheta\f$, \f$\varphi\f$ and \f$r\f$ dependent (complex) function by its multipole coefficients. The class stores the spin-weight of the function and the length. Coefficients are stored for modes l=0,...,l_max (storing coefficients of non-existing modes to make comparison between different spin-weighted functions easier).
class Function
{
public:
    /// \brief Construct Function with a given s and given number of multipole coefficients.
    Function(const int Ys, const SHBases &SHB);
    /// \brief Construct Function with a given s, given number of multipole coefficients and given coefficients.
    Function(const std::vector<Complex> &coeffs, const int Ys, const SHBases &SHB);
    /// \brief Copy-constructor.
    Function(const Function &ff);
    /// \brief A constructor creating a function described by a single multipole coefficient.
    Function(const Complex val, const int s, const int l, const int m, const SHBases &SHB);

    /// \brief Spin-weight getter.
    int spinWeight() const
    {
        return m_Ys;
    }
    /// \brief Lmax getter.
    int lMax() const
    {
         int imax{static_cast<int>(m_coeff.size()-1)};
         return std::floor(std::sqrt(imax));
    }
    int getSize() const
    {
        return static_cast<int>(m_coeff.size());
    }
    /// \brief SHB getter.
    const SHBases& SHB() const
    {
        return m_SHB;
    }
    GReal_t getCoefficientAbs(const int ii) const
    {
        return m_coeff.at(ii).abs();
    }
    GReal_t getCoefficientRe(const int ii) const
    {
        return m_coeff.at(ii).real();
    }
    void setCoefficientToZero(const int ii)
    {
        m_coeff.at(ii) = 0;
    }
    void setCoefficient(const int ii, const Complex z)
    {
        m_coeff.at(ii) = z;
    }

    /// \brief Compute l from i.
    int itol(const int i) const
    {
        return std::floor(std::sqrt(i));
    }
    /// \brief Compute m from i.
    int itom(const int i) const
    {
        int l{itol(i)};
        return i-l*(l+1);
    }
    int lmtoi(const int l, const int m) const
    {
        return l*(l+1)+m;
    }

    /// \brief Overloading output operator.
    friend std::ostream& operator<<(std::ostream &out, const Function &ff);
    /// \brief Overloading input operator.
    friend std::istream& operator>>(std::istream &in, Function &ff);
    /// \brief Multiplication by scalar from left.
    friend Function operator*(const Complex factor, const Function &ff);
    /// \brief Multiplication by scalar from right.
    friend Function operator*(const Function &ff, const Complex factor);
    /// \brief Division by scalar.
    friend Function operator/(const Function &ff, const Complex denominator);
    /// \brief Overload addition operator.
    friend Function operator+(const Function &f1, const Function &f2);
    /// \brief Overload substracion.
    friend Function operator-(const Function &f1, const Function &f2);
    /// \brief Multiplication by spherical harmonics from left.
    friend Function operator*(const SphericalHarmonic &Y, const Function &f);
    /// \brief Division of two Functions using Neumann series.
    friend Function operator/(const Function &f1, const Function &f2);
    /// \brief Multiplication of two functions.
    Function operator*(const Function &f) const;
    /// \brief Overload unary -.
    Function operator-() const;
    /// \brief Assignment.
    Function& operator=(const Function &f);
    /// \brief Not equal operators.
    friend bool operator!=(const Function &f1, const Function &f2);
    /// \brief Equal operators.
    friend bool operator==(const Function &f1, const Function &f2);

    /// \brief Returns the complex conjugate of a function.
    Function bar() const;
    /// \brief Eth operator acting on the Function.
    Function eth() const;
    /// \brief Ethbar operator acting on the Function.
    Function ethbar() const;
    /// \brief Ethbareth operator acting on the Function.
    Function ethbareth() const;
    /// \brief Ethethbar operator acting on the Function.
    Function ethethbar() const;
    /// \brief Ethbar^2 operator acting on the Function.
    Function ethbar2() const;
    Function re() const;
    /// \brief Square \f$L_2\f$ norm of the function.
    GReal_t norm() const;
    /// \brief Returns square of the \f$H_2^2\f$ Sobolev norm.
    GReal_t SobolevNorm2() const;
    /// \brief Produces scaling factor which optimizes the  Neumann diivision.
    Complex optimalScalingNeumann() const;
    /// The minimal norm.
    GReal_t normBoundNeumann() const;
    GReal_t monopole() const
    {
        return m_coeff[0].real();
    }
    friend Function divideByNeumann(const Function& a, const Function& f, const GReal_t norm, const GReal_t tolerance);
    friend Function sqrt(const Function& x);
    friend Function eth(const Function& x);
    friend Function ethbar(const Function& x);
    friend Function ethbareth(const Function& x);
    friend Function ethethbar(const Function& x);
    friend Function ethbar2(const Function& x);
    friend Function Re(const Function& x);
    friend Function Abs2(const Function& x);
private:
    Function sqrtTaylor() const;
    Function sqrtTaylorIterative() const;
    bool checkSqrtError(const Function &res, GReal_t &err, GReal_t &preverr) const;
    GReal_t nextTaylorCoeff(GReal_t &TaylorCoeff, GReal_t &previousOrder) const;
    void getNextTwoTerms(Function &res, Function &dxn, const Function &dx, const GReal_t sqrtNorm, GReal_t &TC, GReal_t &order) const;
    void Taylor4(Function &res) const;
    Function sqrtNewton() const;
    /// \brief Compute size of array based on given lmax.
    int arrayLength(const int lmax) const
    {
        // is this the correct formula?
        return lmax*(lmax+2)+1;
    }
    int computeLMax() const
    {
        int imax{static_cast<int>(m_coeff.size()-1)};
        return std::floor(std::sqrt(imax));
    }
    const SHBases& m_SHB;
    const int m_Ys;
    std::vector<Complex> m_coeff;
};
GReal_t SobolevConstant();
#endif
