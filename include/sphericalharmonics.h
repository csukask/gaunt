///
/// \file sphericalharmonics.h
/// \brief Definition of classes SphericalHarmonic and SHBases.
#ifndef SWSH_H
#define SWSH_H

#include <vector>
#include <cmath>
//#include <unordered_map>
#include "constraint.h"

/// \brief Models a single spherical harmonic.
///
/// In general the l, m and s indices determine the function. However its action on a Function depends on the s' index of this Function. For this reason every single spherical harmonic contains 5 matrices, each one acting on functions with different spin-weight.
class SphericalHarmonic
{
public:
    // need empty constructor for initializing std::vector<SphericalHarmonic>
    SphericalHarmonic()
    {}
    /// \brief Creating a spherical harmonic with s=s, l=l, m=m and a cutoff at l=lmax.
    ///
    /// Given indices s, l, m and a cutoff \f$l_{max}\f$ creates a spin-weighted spherical harmonic. The spherical harmonic is represented by a set of matrices acting on different spin-weight s Function.
    SphericalHarmonic(const int s, const int l, const int m, const int imax, const int lmax);
    /// \brief Producing the Gaunt coefficient specified by s, l, m, si, ii, io and -s-si, where ii and io are composite indices.
    ///
    /// Actually it may be a private function if would not use it for testing purposes.
    /// Here si is the actual spin-weight, not corrected to indexing (the function does the correction).
    GReal_t getGaunt(const int si, const int ii, const int io) const;
    /// \brief Get spin-weight.
    inline int getS() const
    {
        return m_Ys;
    }
    inline int getM() const
    {
        return m_Ym;
    }
private:
    // do we want an abstract base class or global functions to convert between different type of indices?
    // actually global function might be better
    int composit2Yindexl(const int ci) const
    {
        return std::floor(std::sqrt(ci));
    }
    int composit2Yindexm(const int ci, const int Yl) const
    {
        return ci-Yl*(Yl+1);
    }
    int Y2GGGindexl3(const int l1, const int l2, const int l3) const
    {
        return l3-std::abs(l1-l2);
    }
    int Y2GGGindexsm2(const int s2, const int l2, const int l3) const
    {
        return s2+std::min(l2,l3);
    }
    /// \brief Resizing the matrix containing the coefficients.
    void resizeMatrix();
    /// \brief An extra layer to enable debugging.
    GReal_t innerGetGaunt(const int l, const int si, const int ii, const int io) const;
    /// \brief Putting Gaunt coefficients into the matrix.
    void putGaunt(const GReal_t gc, const int l1, const int l2, const int l3, const int m2, const int s2);
    /// \brief Logic looking for trivially vanishing cases.
    bool isGauntVanishing(const int s1, const int s2, const int l1, const int l2, const int l3, const int m1, const int m2, const int m3) const;
    /// \brief Logic telling if we want to store the given coefficient.
    bool isGauntStored(const int s1, const int s2, const int l1, const int l2, const int l3, const int m1, const int m2, const int m3) const;
    /// \brief Spin-weight.
    int m_Ys;
    int m_Ym;
    int m_Yl;
    /// \brief Highest index.
    int m_imax;
    int m_lmax;
    std::vector< std::vector< std::vector< std::vector<GReal_t> > > > m_GGG;
};

/// \brief A collection of spherical harmonics.
///
/// We want to write something like sum+=f[l]*Ys[l].
class SHBases
{
public:
    SHBases(const int lmax=0);
    /// \brief Returns the Yslm associated with the given i(l,m) composite index.
    const SphericalHarmonic& getY(const int s, const int i) const;
    int lMax() const
    {
        return m_lmax;
    }
private:
    /// \brief Given lmax produces imax.
    int computeIMax(const int lmax) const
    {
        return lmax*(lmax+2);
    }
    /// \brief Compute l from i.
    int itol(const int i)
    {
        return std::floor(std::sqrt(i));
    }
    /// \brief Compute m from i.
    int itom(const int i)
    {
        int l{itol(i)};
        return i-l*(l+1);
        //return 0;
    }
    /// \brief Spin-weight.
    int m_s;
    /// \brief \f$l_{max}\f$
    int m_lmax;
    /// \brief \f$i_{max}\f$
    int m_imax;
    std::vector<SphericalHarmonic> m_SH;
};
#endif
