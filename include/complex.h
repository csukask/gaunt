///
/// \file complex.h
/// \brief Stores the definition of class Complex.
#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>
#include "constraint.h"
/// \brief Algebra of complex numbers.
///
/// The most basic building blocks of the data are complex numbers. Some data are real but we handle them as complex for the sake of conformity and possible later applications.
class Complex
{
public:
    /// \brief Constructs a complex number from 0, 1 or 2 real numbers.
    ///
    /// If called without argument initializes to 0. If received one argument convert it to complex number with the given real part. Two arguments are the real and imaginary part.
    /// This way it can cast a GReal to Complex which saves us a lot of typing.
    Complex(const GReal_t re=0, const GReal_t im=0);
    /// \brief Copy-consturctor.
    ///
    /// Creates a Complex from another one by simply copying the real and imaginary parts into the new one.
    Complex(const Complex &c);
    
    /// \brief Printing a complex number as a pair of two real ones.
    ///
    /// Sends the real and imaginary part separated by a space into an ostream. Returns a reference to the same ostream so it can be chained.
    friend std::ostream& operator<<(std::ostream &out, const Complex &c);
    /// \brief Read two real numbers and interpret them as complex number.
    friend std::istream& operator>>(std::istream &in, Complex &c);
    /// \brief Assignment operator.
    ///
    /// Checks against self assignment and returns a reference so it can be chained.
    Complex& operator=(const Complex &c);
    /// \brief Addition of complex numbers.
    ///
    /// Sum of two complex numbers:
    /// \f[
    /// z_1 + z_2 = (\Re z_1 + \Re z_2) + \imath (\Im z_1 + \Im z_2) = z_3
    /// \f]
    /// Implementing it as friend supports the same functionality for either \f$z_1\f$ or \f$z_2\f$ being real.
    friend Complex operator+(const Complex &c1, const Complex &c2);
    /// \brief Substraction of complex numbers.
    ///
    /// Difference of two complex numbers:
    /// \f[
    /// z_1 - z_2 = (\Re z_1 - \Re z_2) + \imath (\Im z_1 - \Im z_2) = z_3
    /// \f]
    /// Implementing it as friend supports the same functionality for either \f$z_1\f$ or \f$z_2\f$ being real.
    friend Complex operator-(const Complex &c1, const Complex &c2);
    /// \brief Multiplication of complex numbers.
    ///
    /// Product of two complex numbers:
    /// \f[
    /// z_1 \cdot z_2 = (\Re z_1 \cdot \Re z_2 - \Im z_1 \cdot \Im z_2) + \imath (\Re z_1 \cdot \Im z_2 + \Im z_1 \cdot \Re z_2) = z_3
    /// \f]
    /// Implementing it as friend supports the same functionality for either \f$z_1\f$ or \f$z_2\f$ being real.
    friend Complex operator*(const Complex &c1, const Complex &c2);
    /// \brief Division of complex numbers.
    ///
    /// Quotient of two complex numbers:
    /// \f[
    /// \frac{z_1}{z_2} = \frac{(\Re z_1 \cdot \Re z_2 + \Im z_1 \cdot \Im z_2) + \imath (\Im z_1 \cdot \Re z_2 - \Re z_1 \cdot \Im z_2)}{(\Re z_2)^2+(\Im z_2)^2} = z_3
    /// \f]
    /// Implementing it as friend supports the same functionality for either \f$z_1\f$ or \f$z_2\f$ being real.
    friend Complex operator/(const Complex &c1, const Complex &c2);
    
    /// \brief Unary -.
    ///
    /// Returns the Complex times \f$-1\f$.
    Complex operator-() const;
    /// \brief Not equal.
    friend bool operator!=(const Complex &c1, const Complex &c2);
    
    /// \brief Get real part.
    GReal_t real() const
    {
        return m_re;
    }
    
    /// \brief Complex conjugate.
    ///
    /// Returns the Complex \f$\Re z - \imath\Im z\f$.
    Complex conjugate() const;
    /// \brief Absolute value /modulus.
    ///
    /// Returns the modulus \f$\sqrt{(\Re z)^2+(\Im z)^2}\f$. Not sure if we really have to take the square root for our application.
    GReal_t abs() const;
    /// \brief Absolute value /modulus squared.
    ///
    /// Returns the modulus squared \f$|z|^2=\overline{z}z=(\Re z)^2+(\Im z)^2\f$.
    GReal_t abs2() const;
private:
    /// Real part of the number.
    GReal_t m_re;
    /// Imaginary part of the number.
    GReal_t m_im;
};

#endif
