## Product of functions using Gaunt coefficients

This snippet considers complex functions on the sphere given by their spherical harmonic expansion coefficient truncated at a given maximal l value. Expansion coefficients of their products can be computed using Gaunt coefficients.

This code is an excerpt from ConstraintSolver of [sw-gridripper project](http://www.rmki.kfki.hu/~gridripper/) for educational purposes.

Unit testing is done using [Catch2](https://github.com/catchorg/Catch2/tree/v2.x).

Wigner 3j symbols are computed using [wigxjpf library](http://fy.chalmers.se/subatom/wigxjpf/). Preprint paper on the math is [here](https://arxiv.org/abs/1504.08329).

Both of the above libraries are in the repository.

# Build instructions

We need g++, make and cmake to build.

We use cmake to generate makefiles. To start an out of source build create a build directory in the source directory

`mkdir build`

Then move into this newly created directory

`cd build`

And generate the makefiles referring to the directory containing the main CMakeListst.txt file.

`cmake ..`

Now that the makefiles are generated we can build

`make`

and run the unit tests using

`ctest`
